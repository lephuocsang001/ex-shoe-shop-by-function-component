import React from "react";

export default function ItemShoe({ shoe, handleAddToCart }) {
  let { image, name, price } = shoe;
  return (
    <div className="card col-3">
      <img src={image} className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">{name}</h5>
        <p className="card-text">{price}$</p>
        <button
          onClick={() => {
            handleAddToCart(shoe);
          }}
          className="btn btn-primary"
        >
          Add
        </button>
      </div>
    </div>
  );
}
