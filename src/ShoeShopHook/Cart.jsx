import React from "react";

export default function Cart({ cart, handleDelete, handleChangeQuantity }) {
  let renderTbody = () => {
    return cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}$</td>
          <td>
            <button
              onClick={() => {
                handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <strong>{item.soLuong}</strong>
            <button
              onClick={() => {
                handleChangeQuantity(item.id, +1);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: 50 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                handleDelete(item.id);
              }}
              className="btn btn-danger"
            >
              Xoa
            </button>
          </td>
        </tr>
      );
    });
  };
  return (
    <div>
      <h2 style={{ textAlign: "center" }}>Cart</h2>
      <div>
        <table className="table">
          <thead>
            <tr>
              <td>ID</td>
              <td>Name</td>
              <td>Price</td>
              <td>So luong</td>
              <td>Image</td>
              <td>Action</td>
            </tr>
          </thead>
          <tbody>{renderTbody()}</tbody>
        </table>
      </div>
    </div>
  );
}
