import React from "react";
import ItemShoe from "./ItemShoe";

export default function ListShoe({ data, handleAddToCart }) {
  return (
    <div>
      <h2 style={{ textAlign: "center" }}>ListShoe</h2>
      <div className="row">
        {data.map((shoe, index) => {
          return (
            <ItemShoe
              key={index}
              shoe={shoe}
              handleAddToCart={handleAddToCart}
            />
          );
        })}
      </div>
    </div>
  );
}
